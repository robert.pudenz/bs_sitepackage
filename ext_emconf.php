<?php

/**
 * Extension Manager/Repository config file for ext "bs_sitepackage".
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'BS Sitepackage',
    'description' => 'Bootstrap Sidepackage',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
            'fluid_styled_content' => '9.5.0-9.5.99',
            'rte_ckeditor' => '9.5.0-9.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Pudenzone\\BsSitepackage\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Robert Pudenz',
    'author_email' => 'robert_pudenz@web.de',
    'author_company' => 'PudenZ.One',
    'version' => '1.0.0',
];
